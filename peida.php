<?php
/**
 * Plugin Name:       Peida
 * Plugin URI:        https://gitlab.com/implenton/wordpress/peida
 * Description:       Move pages to a new tab on All Pages screen
 * Version:           1.3.0
 * Author:            implenton
 * Author URI:        https://implenton.com/
 * License:           GPLv3
 * License URI:       https://www.gnu.org/licenses/gpl-3.0.txt
 * GitLab Plugin URI: implenton/wordpress/peida
 */

namespace Peida;

if ( ! defined( 'WPINC' ) ) {
    die;
}

add_filter( 'views_edit-page', __NAMESPACE__ . '\subsubsub_insert_tab', 10, 1 );
add_filter( 'pre_get_posts', __NAMESPACE__ . '\filter_pages', 10, 1 );

function subsubsub_insert_tab( $views ) {
    $pages    = _get_pages();
    $pages_nr = count( $pages );

    if ( $pages_nr < 1 ) {
        return $views;
    }

    $url = add_query_arg( [
        'post_type'   => 'page',
        'peida_pages' => 1,
    ], get_admin_url( null, 'edit.php' ) );

    $views[] = sprintf(
        '<a href="%s">%s</a><span class="count">(%s)</span>',
        esc_url( $url ),
        apply_filters( 'peida_tab_label', esc_html__( 'Peida Pages', 'peida' ) ),
        esc_attr( $pages_nr )
    );

    return $views;
}

function filter_pages( $query ) {
    $filtered_page_ids = _get_pages();

    if ( empty( $filtered_page_ids ) ) {
        return $query;
    }

    if ( _is_edit_page_screen() ) {
        $query->query_vars['post__not_in'] = $filtered_page_ids;
    }

    if ( _is_edit_page_screen() && isset( $_REQUEST['peida_pages'] ) ) {
        $query->query_vars['post__in'] = $filtered_page_ids;
    }

    return $query;
}

function _is_edit_page_screen() {
    global $pagenow, $post_type;

    return is_admin() && $pagenow == 'edit.php' && $post_type == 'page' ? true : false;
}

function _get_pages() {
    $included_list = apply_filters( 'peida_pages', [] );
    $pages_id      = array_filter( array_map( function ( $item ) {
        return _maybe_guess_page_id( $item );
    }, $included_list ) );

    return $pages_id;
}

function _maybe_guess_page_id( $item ) {
    if ( is_int( $item ) ) {
        return $item;
    }

    if ( ! is_string( $item ) ) {
        return false;
    }

    if ( ( $page_id = _try_guess_page_id_from_slug( $item ) ) !== false ) {
        return $page_id;
    }

    if ( ( $page_id = _try_guess_page_id_from_template_path( $item ) ) !== false ) {
        return $page_id;
    }

    return false;
}

function _try_guess_page_id_from_slug( $slug ) {
    $found_page = get_page_by_path( $slug );

    return ! is_null( $found_page ) ? $found_page->ID : false;
}

function _try_guess_page_id_from_template_path( $template_path ) {
    if ( strpos( $template_path, '.php' ) === false ) {
        return false;
    }

    remove_filter( 'pre_get_posts', __NAMESPACE__ . '\filter_pages' );

    $page_with_template_query = new \WP_Query( [
        'post_type'              => 'page',
        'meta_key'               => '_wp_page_template',
        'posts_per_page'         => 1,
        'meta_value'             => $template_path,
        'no_found_rows'          => true,
        'update_post_meta_cache' => false,
        'update_post_term_cache' => false,
    ] );

    wp_reset_query();

    add_filter( 'pre_get_posts', __NAMESPACE__ . '\filter_pages' );

    return ! $page_with_template_query->posts ? false : $page_with_template_query->posts[0]->ID;
}